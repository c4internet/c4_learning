angular.module('eLearning')
  .directive('eAvatar', function ($timeout) {
  
  return function(scope, element, attr){

    $timeout(function() {
        console.log("entrou");
        element.find(".animate").removeClass("animate");
    }, parseInt(attr.eAvatar) * 1000);
  }
  
});