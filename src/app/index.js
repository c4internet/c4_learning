'use strict';

angular.module('eLearning', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ngRoute', 'ngMaterial', 'ngAudio',"com.2fdevs.videogular",
    "com.2fdevs.videogular.plugins.controls","com.2fdevs.videogular.plugins.overlayplay","com.2fdevs.videogular.plugins.poster","ngMdIcons"])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .when('/preview', {
        templateUrl: 'app/preview/preview.html',
        controller: 'PreviewCtrl'
      })
      .when('/go/:param', {
          templateUrl: 'app/preview/preview.html',    
          controller: 'PreviewCtrl'
      }).
      otherwise({
        redirectTo: '/'
      });
  })
;
