'use strict';

angular.module('eLearning')
  .controller('MainCtrl', function ($scope, Video, localStorage, $timeout,$routeParams, EditorNavigate, Navigate, Sound, eMarkdown, eEditor) {

    localStorage.localVar("c4-lerning");

    Video.setVideo("assets/videos/videogular.webm");
    $scope.video = Video.getVideo();

    $scope.image_size = {'width':1600, 'height':700}

    var go_to_page = 0;

    if($routeParams.param){

      var pg = parseInt($routeParams.param) -1;

      go_to_page = pg;
    }

    var md = angular.element('#md_template').html();
      
    var scorm = pipwerks.SCORM;
    
    $scope.slides = eMarkdown.init(md);  

    $scope.highlightText = function(){
      angular.element(".slide.selected .e-highlight").addClass("correct");
	  
      if(gotoAndPlay()){
        Navigate.change("next");
      }else{
        angular.element(".slide.selected .e-highlight").removeClass("correct");
      }
    }
    
    $scope.radio = function(value){      
      
      if(angular.element(".slide.selected .e-radio").attr('correct') == value){
      
        angular.element(".slide.selected .e-radio").addClass("correct");
        
        if(gotoAndPlay()){
          Navigate.change("next");
        }
        
      }else{
        angular.element(".slide.selected .e-radio").removeClass("correct");
      }
    }
    
    $scope.select = function(value){
      
      if(angular.element(".slide.selected .e-select").attr("correct") == value){
        angular.element(".slide.selected .e-select").addClass("correct");
        
        if(gotoAndPlay()){
          Navigate.change("next");
        }
        
      }else{
        angular.element(".slide.selected .e-select").removeClass("correct");
      }
    }
	
    $scope.checkbox = function(value){
      
      if(value){
        if(gotoAndPlay()){
          Navigate.change("next");
        }
      }
    }
	
	function gotoAndPlay(){
		
		//highlight
		if(angular.element(".slide.selected .e-highlight").length > 0){
			if(angular.element(".slide.selected .e-highlight.correct").length == 0){
				return false;
			}
		}
		
		//radio
		if(angular.element(".slide.selected .e-radio").length > 0){
			if(angular.element(".slide.selected .e-radio.correct").length == 0){
				return false;
			}
		}
		
		//checkbox
    if(angular.element(".slide.selected .e-checkbox").length > 0){
			if((angular.element(".slide.selected .e-checkbox").length -1) != angular.element(".slide.selected .e-checkbox.md-checked").length){
			  return false;
			} 
		}
		//text
		if(angular.element(".slide.selected .e-text").length > 0){
			if(angular.element(".slide.selected .e-text").length != angular.element(".slide.selected .e-text.correct").length){
			  return false;
			}
		}	
		

		//select
		if(angular.element(".slide.selected .e-select").length > 0){
			if(angular.element(".slide.selected .e-select").length != angular.element(".slide.selected .e-select.correct").length){
			  return false;
			}
		}	

		return true;
		
	};

    $scope.selected = function(temp) {
    
        var cssClass = parseInt(Navigate.getPage()) == parseInt(temp) ? 'selected' : '';
        
        return cssClass;
    };
    
    $scope.keyPress = function(e){
      
      if( angular.element(e.currentTarget).val() == angular.element(e.currentTarget).attr("correct") ){
        angular.element(e.currentTarget).addClass("correct");
        
        if(gotoAndPlay()){
          Navigate.change("next");
        }
		
      }else{
        angular.element(e.currentTarget).removeClass("correct");
      }
       
    }
    
    $scope.init = function(){
      console.log(angular.element(".slide.selected .e-audio").length);
    
      if(angular.element(".slide.selected .e-audio").length > 0){
        Sound.start(angular.element(".slide.selected .e-audio").attr("id"));
        Sound.play();
      }
    }
            
    $scope.slide_height = angular.element(window).height() - 50;
    
    angular.element( window ).resize(function() {
      resize();
    });

    resize();

    function resize(){
      $scope.slide_height = angular.element(window).height() - 50;

      angular.element(".e-background").height(angular.element(window).height() - 50);
      
      angular.element(".slide.selected .e-content").css("min-height", ( angular.element(window).height() - 50 ) + "px");

      angular.element(".e-tab ul").height(angular.element(window).height());
      angular.element(".e-tab .e-tab-content").height(angular.element(window).height());
    }
    
    $scope.tip = function(e){
      
      if(angular.element(e.currentTarget).parent().find('p').css("display") == "none"){      
        angular.element(e.currentTarget).parent().find('p').css("display", "block");
        
        TweenMax.to(angular.element(e.currentTarget).find('span'),1,{rotation:135,ease:Expo.easeOut});
        
      }else{
        angular.element(e.currentTarget).parent().find('p').css("display", "none");
        TweenMax.to(angular.element(e.currentTarget).find('span'),1,{rotation:0,ease:Expo.easeOut});
      }
    }
    
    $scope.tab = function(e){
      var content = angular.element(e.currentTarget).find("p").html();
      
      angular.element(".e-tab-content").addClass("selected");
      angular.element(".e-tab-content").html(content);
      
      angular.element(e.currentTarget).parent().find("li").removeClass("selected");
      
      angular.element(e.currentTarget).addClass("selected");
      
      
    }

    scorm.version = "1.2";
    
    var lmsConnected = scorm.init();
    var lessonStatus = "";
    var success = "";
    var location = "";
    
    if(lmsConnected){
    
      lessonStatus = scorm.get("cmi.core.lesson_status");
      
      if(lessonStatus == "completed"){
        scorm.disconnect();
      } else {       
        success = scorm.set("cmi.core.lesson_status", "incomplete");
        scorm.save();
      
        if(success) {

          location = parseInt(scorm.get("cmi.core.lesson_location"));
          
          if(location) {
            go_to_page = location;
          }else{
            go_to_page = 0;
          }
        }      
      }
      
    } else {
      console.log("Could not connect to LMS.");
    }		
    
    //editor desativa o go to
    go_to_page = 0;

    Navigate.setPage(go_to_page);

    eEditor.init($scope); 

    $timeout(function(){
      eEditor.getVar().setValue(eEditor.getSlide(go_to_page));
    },1);
    

    $scope.page = go_to_page + 1;
    $scope.page_length = eEditor.getSlides().length;
    
    $scope.editorNext = function(){
      EditorNavigate.change("next");
      $scope.page = EditorNavigate.getHumanPage();
    }

    $scope.editorBack = function(){
      EditorNavigate.change("prev"); 
      $scope.page = EditorNavigate.getHumanPage(); 
    }

    $scope.next = function(){
      Navigate.change("next");
    }

    $scope.prev = function(){
      Navigate.change("prev");
    }

    $scope.addInputText = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-input-text").html());

      return false;
    }

    $scope.addBlock1 = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-content-block-1").html());

      return false;
    }

    $scope.addNew = function(){

      var temp_slides = JSON.parse(localStorage.get("md"));

      var add_md = angular.element(".template-new-page").html();

      var obj = eMarkdown.init(add_md);
      
      temp_slides.splice((parseInt(EditorNavigate.getPage()) + 1), 0, obj[0]);

      localStorage.set("md",JSON.stringify(temp_slides));

      eEditor.setSlides(temp_slides);

      EditorNavigate.change("next");
      $scope.page = EditorNavigate.getHumanPage();

      return false;
    }

    $scope.videoNew = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-video-page").html());

      return false;
    }

    $scope.audioNew = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-audio-page").html());

      return false;
    }

    $scope.avatarNew = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-avatar-page").html());

      return false;
    }

    $scope.highlightNew = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-highlight-page").html());

      return false;
    }

    $scope.addBackground = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-back-page").html());

      return false;
    }

    $scope.addCoverLogo = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-cover-logo-page").html());

      return false;
    }

    $scope.addCoverText = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-cover-text-page").html());

      return false;
    }

    $scope.addRedBox = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-red-box").html());

      return false;
    }

    $scope.addBlackBox = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-black-box").html());

      return false;
    }

    $scope.addCover = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-back-cover-page").html());

      return false;
    }

    $scope.addLegend = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-legend-page").html());

      return false;
    }

    $scope.addTip = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-tip-page").html());

      return false;
    }

    $scope.addAnimation = function(){

      eEditor.getVar().replaceSelection(angular.element(".template-page-animation-page").html());

      return false;
    }



    var s = JSON.parse(localStorage.get("md"));

    $scope.legend = s[0].legend;

    //angular.element(".legend-text p").html("teste");
  });