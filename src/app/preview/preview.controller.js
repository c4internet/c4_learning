'use strict';

angular.module('eLearning')
  .controller('PreviewCtrl', function ($scope, Video, utils, localStorage, $timeout,$routeParams, Navigate, Sound, $sce) {

    localStorage.localVar("c4-lerning");
    
    $scope.image_size = {'width':1600, 'height':700}
    
    Video.setVideo("assets/videos/videogular.webm");
    $scope.video = Video.getVideo();
    
    var go_to_page = 0;

    if($routeParams.param){

      var pg = parseInt($routeParams.param) -1;

      go_to_page = pg;
    }

    if(!utils.nil(localStorage.get("md"))){ return false; }
    
    $scope.slides = JSON.parse(localStorage.get("md"));

    $scope.highlightText = function(){
      angular.element(".slide.selected .e-highlight").addClass("correct");
    
      if(gotoAndPlay()){
        Navigate.change("next");
      }else{
        angular.element(".slide.selected .e-highlight").removeClass("correct");
        }
      }
    
    $scope.radio = function(value){      
      
      if(angular.element(".slide.selected .e-radio").attr('correct') == value){
      
        angular.element(".slide.selected .e-radio").addClass("correct");
        
        if(gotoAndPlay()){
          Navigate.change("next");
        }
        
      }else{
        angular.element(".slide.selected .e-radio").removeClass("correct");
      }
    }
    
    $scope.select = function(value){
      
      if(angular.element(".slide.selected .e-select").attr("correct") == value){
        angular.element(".slide.selected .e-select").addClass("correct");
        
        if(gotoAndPlay()){
          Navigate.change("next");
        }
        
      }else{
        angular.element(".slide.selected .e-select").removeClass("correct");
      }
    }
  
    $scope.checkbox = function(value){
      
      if(value){
        if(gotoAndPlay()){
          Navigate.change("next");
        }
      }
    }
  
  function gotoAndPlay(){
    
    //highlight
    if(angular.element(".slide.selected .e-highlight").length > 0){
      if(angular.element(".slide.selected .e-highlight.correct").length == 0){
        return false;
      }
    }
    
    //radio
    if(angular.element(".slide.selected .e-radio").length > 0){
      if(angular.element(".slide.selected .e-radio.correct").length == 0){
        return false;
      }
    }
    
    //checkbox
    if(angular.element(".slide.selected .e-checkbox").length > 0){
      if((angular.element(".slide.selected .e-checkbox").length -1) != angular.element(".slide.selected .e-checkbox.md-checked").length){
        return false;
      } 
    }
    //text
    if(angular.element(".slide.selected .e-text").length > 0){
      if(angular.element(".slide.selected .e-text").length != angular.element(".slide.selected .e-text.correct").length){
        return false;
      }
    } 
    

    //select
    if(angular.element(".slide.selected .e-select").length > 0){
      if(angular.element(".slide.selected .e-select").length != angular.element(".slide.selected .e-select.correct").length){
        return false;
      }
    } 

    return true;
    
  };

    $scope.selected = function(temp) {
    
        var cssClass = parseInt(Navigate.getPage()) == parseInt(temp) ? 'selected' : '';
        
        return cssClass;
    };
    
    $scope.keyPress = function(e){
      
      if( angular.element(e.currentTarget).val() == angular.element(e.currentTarget).attr("correct") ){
        angular.element(e.currentTarget).addClass("correct");
        
        if(gotoAndPlay()){
          Navigate.change("next");
        }
    
      }else{
        angular.element(e.currentTarget).removeClass("correct");
      }
       
    }
    
    $scope.init = function(){
      console.log(angular.element(".slide.selected .e-audio").length);
    
      if(angular.element(".slide.selected .e-audio").length > 0){
        Sound.start(angular.element(".slide.selected .e-audio").attr("id"));
        Sound.play();
      }
    }
            
    $scope.slide_height = angular.element(window).height() - 50;
    
    angular.element( window ).resize(function() {
      resize();
    });

    resize();

    function resize(){
      $scope.slide_height = angular.element(window).height() - 50;

      angular.element(".e-background").height(angular.element(window).height() - 50);
      
      angular.element(".slide.selected .e-content").css("min-height", ( angular.element(window).height() - 50 ) + "px");

      angular.element(".e-tab ul").height(angular.element(window).height());
      angular.element(".e-tab .e-tab-content").height(angular.element(window).height());
    }
    
    $scope.tip = function(e){
      
      if(angular.element(e.currentTarget).parent().find('p').css("display") == "none"){      
        angular.element(e.currentTarget).parent().find('p').css("display", "block");
        
        TweenMax.to(angular.element(e.currentTarget).find('span'),1,{rotation:135,ease:Expo.easeOut});
        
      }else{
        angular.element(e.currentTarget).parent().find('p').css("display", "none");
        TweenMax.to(angular.element(e.currentTarget).find('span'),1,{rotation:0,ease:Expo.easeOut});
      }
    }
    
    $scope.tab = function(e){
      var content = angular.element(e.currentTarget).find("p").html();
      
      angular.element(".e-tab-content").addClass("selected");
      angular.element(".e-tab-content").html(content);
      
      angular.element(e.currentTarget).parent().find("li").removeClass("selected");
      
      angular.element(e.currentTarget).addClass("selected");
      
      
    }
    var scorm = pipwerks.SCORM;

    scorm.version = "1.2";
    
    var lmsConnected = scorm.init();
    var lessonStatus = "";
    var success = "";
    var location = "";
    
    if(lmsConnected){
    
      lessonStatus = scorm.get("cmi.core.lesson_status");
      
      if(lessonStatus == "completed"){
        scorm.disconnect();
      } else {       
        success = scorm.set("cmi.core.lesson_status", "incomplete");
        scorm.save();
      
        if(success) {

          location = parseInt(scorm.get("cmi.core.lesson_location"));
          
          if(location) {
            go_to_page = location;
          }else{
            go_to_page = 0;
          }
        }      
      }
      
    } else {
      console.log("Could not connect to LMS.");
    }   
    

    $scope.next = function(){
      Navigate.change("next");
    }

    $scope.prev = function(){
      Navigate.change("prev");
    }

    
    Navigate.goTo(go_to_page);  

  });