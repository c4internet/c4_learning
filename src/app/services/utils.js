angular.module('eLearning')
  .service('utils', function($filter) {
    this.nil = function(temp){
      if(temp != "" && temp != null && temp != undefined){
        return true;
      }else{
        return false;
      }
    },
    this.consume_average = function(arr){

      var sum = 0;

      if(arr.length > 0){
        for(var i = 0; i < arr.length; i++){
          sum += parseFloat(arr[i].consume);
        }
      }

      return parseFloat(sum / arr.length);

    },

    this.getMonth = function(data){

      return this.getMonthName(data.split("/")[0]);

    },

    this.getYear = function(data){

      return data.split("/")[1];

    },

    this.getConsume = function(data){

      return data.replace("m³", "");

    },

    this.getMonthName = function(month){
      var monthNames = ["","Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
  "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];

      return monthNames[parseInt(month)];

    },

    this.isSameDate = function(date01, date02){
      return this.getDateInfo(date01, "MMyyyy") == this.getDateInfo(date02, "MMyyyy");
    },

    this.getDateInfo = function(date, option){
      return $filter('date')(date, option);
    }

    this.differenceInDays = function(date01, date02){
      date01 = new Date(date01)
      date02 = new Date(date02);
      
      var timeDiff = Math.abs(date02.getTime() - date01.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

      return diffDays;
    },

    this.object_to_md = function(obj){

      var md = "";

      //title
      md += "\n#" + obj.title + "\n";

      //class
      if(this.nil(obj.class)){
        md += "\n##" + obj.class + "\n";
      }

      //legend
      if(this.nil(obj.legend)){
        md += "\n###" + obj.legend + "\n";
      }


      //img
      if(this.nil(obj.img)){
        md += '![' + obj.img.alt + '](' + obj.img.src + ' "' + obj.img.title + '")';
      }

      var $this = this;

      obj.code.forEach(function(data) {

        md += "\n```\n";

        if($this.nil(data.type)){
          md += "-type: "+ data.type +"\n";
        }

        if($this.nil(data.class)){
          md += "-class: "+ data.class +"\n";
        }

        if($this.nil(data.image)){
          md += "-image: "+ data.image +"\n";
        }

        if($this.nil(data.description)){
          md += "-description: "+ data.description +"\n";
        }

        if($this.nil(data.animation)){
          md += "-animation: "+ data.animation +"\n";
        }

        if($this.nil(data.position)){
          md += "-position: [" + data.position[0] + "," + data.position[1] + "]\n";
        }

        if($this.nil(data.size)){
          md += "-size: [" + data.size[0] + "," + data.size[1] + "]\n";
        }

        if($this.nil(data.labels)){
          md += "-labels: [";

          data.labels.forEach(function(d) {
            md += d;
            if(data.labels.indexOf(d) < (data.labels.length -1)){
              md += ", ";
            }

          });

          md += "]\n";
        }

        if($this.nil(data.titles)){
          md += "-titles: [";

          data.titles.forEach(function(d) {
            md += d;
            if(data.titles.indexOf(d) < (data.titles.length -1)){
              md += ", ";
            }

          });

          md += "]\n";
        }

        if($this.nil(data.descriptions)){
          md += "-descriptions: [";

          data.descriptions.forEach(function(d) {
            md += d;
            if(data.descriptions.indexOf(d) < (data.descriptions.length -1)){
              md += ", ";
            }

          });

          md += "]\n";
        }
        if($this.nil(data.delay)){
          md += "-delay: "+ data.delay +"\n";
        }

        if($this.nil(data.time)){
          md += "-time: "+ data.time +"\n";
        }

        if($this.nil(data.stop)){
          md += "-stop: "+ data.stop +"\n";
        }

        if($this.nil(data.avatar)){
          md += "-avatar: "+ data.avatar +"\n";
        }

        if($this.nil(data.audio)){
          md += "-audio: "+ data.audio +"\n";
        }

        if($this.nil(data.mode)){
          md += "-mode: "+ data.mode +"\n";
        }

        md += "```\n";

      });
  
      return md;
    }
    
  });