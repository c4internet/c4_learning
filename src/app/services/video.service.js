angular.module('eLearning')
  .service('Video', function($sce) {
    var url = "";

    this.setVideo = function(temp){
      url = temp;
    },

    this.getVideo = function(){
      return {
          preload: "none",
          sources: [            
              {src: $sce.trustAsResourceUrl(url), type: "video/webm"}
          ]
      };
    }
    
  });
