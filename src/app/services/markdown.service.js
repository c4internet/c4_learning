'use strict';

angular.module('eLearning')
  .service('eMarkdown', function() {
    
    this.init = function(md){

      var out = markdown.toHTMLTree(md);        
      var slides = {};
      slides["slides"] = new Array();
      var slide = {};
      var i = 0;      
      var $this = this;

      out.forEach(function(data) {      

        if(data[0] == "h1"){

          if(slide.title != undefined ){
            slides["slides"].push(slide);
          }

          slide = {};
          slide["etext"] = "";
          slide["code"] = new Array();

          slide["title"] = data[1];
          slide["etext"] += "\n#" + data[1] + "\n"
        }
        
        if(data[0] == "h2"){
          slide["class"] = data[1];
          slide["etext"] += "\n##" + data[1] + "\n"
        }

        if(data[0] == "h3"){
          slide["legend"] = data[1];
          slide["etext"] += "\n###" + data[1] + "\n"
        }
        
        if(data[0] == "p"){ 
          data.forEach(function(d) {

            if(d[0] == "img"){ 
              slide["img"] = d[1];
              slide["etext"] += '![' + d[1].alt + '](' + d[1].src + ' "' + d[1].title + '")';
              
            }else if(d[0] == "code"){
              slide["code"].push($this.get_code(d[1]));
              slide["etext"] += "\n```" + d[1] + "```\n";
            }else{
              slide["description"] = data[1];
              //slide["etext"] += "\n" + data[1];
            }
          });        
        }        

        i += 1;

        if(out.length <= i){
          slides["slides"].push(slide);
        }

      });
      
      //legenda
      //angular.element(
      if (angular.element(".slide.selected")[0]){
        //console.log(angular.element(".slide.selected p").html());
      }
      
      return slides.slides;

    },

    this.get_code = function(code){
      var props = {};
      var lines = code.split('\n');
      for (var i = 0; i < lines.length; ++i) {
        var e = /^-([^:]+):(.*)/.exec(lines[i].replace(/^\s+|\s+$/gm,''));
        if (e) {
          try {
            var first = e[1].replace(/^\s+|\s+$/gm,'');
            var second = e[2].replace(/^\s+|\s+$/gm,'');

            if(first == "position" || first == "size" || first == "labels" || first == "titles" || first == "descriptions"){
              props[first] = JSON.parse(second);
            }else{
              props[first] = second;
            }
          } catch(e) {
          }
        }
      }
      return props;
    }   
    
    
  });
