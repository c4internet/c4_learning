angular.module('eLearning')
  .service('EditorNavigate', function(eEditor, localStorage, utils) {
    var currentPage = 0;
    
    this.getPage = function(){
      return currentPage;
    },

    this.getHumanPage = function(){
      return currentPage + 1;
    },
    
    this.setPage = function(page){
      currentPage = parseInt(page) - 1;

      eEditor.setCurrentPage(currentPage);
    },

    this.change = function(go){ 
          
      var temp = eEditor.getSlides();
      
      if(go == "next"){
        currentPage += 1;
        
        if(currentPage > temp.length - 1){
          currentPage = temp.length - 1;
        }

        this.titleUpdate();
        
        this.legendUpdate();

        eEditor.setCurrentPage(currentPage);

        eEditor.getVar().setValue(eEditor.getSlide(currentPage));

      }else if(go == "prev"){
        currentPage -= 1;

        if(currentPage < 0){
          currentPage = 0;          
        }

        this.titleUpdate();

        this.legendUpdate();

        eEditor.setCurrentPage(currentPage);
        
        eEditor.getVar().setValue(eEditor.getSlide(currentPage));
      }


      

    },

    this.legendUpdate = function(){
      
      localStorage.localVar("c4-lerning");

      var s = JSON.parse(localStorage.get("md"));
      angular.element(".legend").addClass("hide");
      angular.element(".legend").removeClass("show");
      
      if(utils.nil(s[currentPage].legend)){
        angular.element(".legend").removeClass("hide");
        angular.element(".legend").addClass("show");
        angular.element(".legend-text p").html(s[currentPage].legend);
      }
      
    },

    this.titleUpdate = function(){
      
      localStorage.localVar("c4-lerning");

      var s = JSON.parse(localStorage.get("md"));
            
      if(utils.nil(s[currentPage].title)){
        angular.element(".head-title p").html(s[currentPage].title);
      }
      
    }
});